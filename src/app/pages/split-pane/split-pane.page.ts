import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
//import { title } from 'process';
@Component({
  selector: 'app-split-pane',
  templateUrl: './split-pane.page.html',
  styleUrls: ['./split-pane.page.scss'],
})
export class SplitPanePage implements OnInit {

  pages=[
    {
      title:'Tabs',
      url:'/tab1',
    },
    {
      title:'Home',
      url:'/homepage',
    },
    {
      title:'Login',
      url:'/login',
    },
 

  ];

  selectedPath='';
  constructor(
    private router:Router,
  ) { 
    this.router.events.subscribe ((event:RouterEvent) => {
      if (event && event.url) { 
        this.selectedPath=event.url; 
      }
    });
  }
  
  ngOnInit() {
  }

}
