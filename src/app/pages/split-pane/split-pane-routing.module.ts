import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SplitPanePage } from './split-pane.page';

const routes: Routes = [
  {
    path: '',
    component: SplitPanePage,
    children: [
      {
        path:'tabs-manager',
        loadChildren: () =>import('../tabs-manager/tabs-manager.module').then(m => m.TabsManagerPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SplitPanePageRoutingModule {}
