import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsManagerPage } from './tabs-manager.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsManagerPage,
    children:[
      {
        path: 'tab1',
        loadChildren: () => import('../tab1/tab1.module').then (m => m.Tab1PageModule)
      }
    ]
  },
  {
  path: '',
  redirectTo:'tabs/tab1',
  pathMatch:'full'
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsManagerPageRoutingModule {}
